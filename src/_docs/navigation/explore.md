---
title: Explore
category: Navigation
order: 1
requirements:
  build: Any
  plan: Free
  hosting: Any
---

Explore allows navigation of all pages, collections and data in your site. This generates automatically based on your site configuration. Images and titles come from the corresponding file's front matter.

![Source Editor](/uploads/screen-shot-2018-07-25-at-9-09-37-am.png){: .screenshot srcset="/uploads/screen-shot-2018-07-25-at-9-09-37-am.png 800w, /uploads/screen-shot-2018-07-25-at-9-09-37-am@2x.png 1600w"}

### Pages vs Data

Collections display in two categories. If a collection has `output: true` it appears in the pages section. Otherwise, the collection appears with data.